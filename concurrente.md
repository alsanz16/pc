# Tema 1
## Independencia de procesos
Decimos que dos procesos son independientes si su ejecución llega siempre al mismo estado, con independencia del orden en el que se ejecuten las distintas instrucciones de los mismos.

Una condición suficiente para la independencia es la siguiente: dado un proceso P definimos como W(P) al conjunto de variables escritas por el proceso P y R(P) al conjunto de variables leídas por A. Entonces, si $$W(A) \cap (R(B) \cup W(B)) = \varnothing$$ $$W(B) \cap (R(A) \cup W(A)) = \varnothing$$ los procesos son independientes con toda seguridad

### Ejemplo: producto de matrices
Queremos calcular el producto de las matrices $A$ y $B$. El programa secuencial es:

    1- for i = 0 to N-1
    2- 		for i = 0 to N-1
    3- 			c[i,j] = 0
    4- 			for k = 0 to N-1
    5- 				c(i,j) = c(i,j) + A[i,k] * B[k,j]

Llamemos $P(i,j)$ al proceso que ejecuta las líneas $3,4,5$ para una determinada elección de (i,j).

Tenemos que:
$$R(P(i,j)) = \{c[i,j], A[i, *], B[*,j]\}$$ 
$$W(P(i,j)) = \{ c[i,j] \} $$
con lo que si $(i,j) \neq (i', j')$ entonces los procesos $P(i,j)$ y $P(i', j')$ son independientes por el criterio anterior y son susceptibles a una aproximación concurrente.

#### Concurrencia por filas
Para utilizar concurrencia por filas, podemos hacer el siguiente cambio:

    1- co(i = 0 to N-1)
    2- 		for i = 0 to N-1
    3- 			c[i,j] = 0
    4- 			for k = 0 to N-1
    5- 				c(i,j) = c(i,j) + A[i,k] * B[k,j]
       oc
                    
de tal manera que creemos un proceso que se encargue del cálculo de cada fila.

#### Concurrencia por columnas
Para hacerlo por columnas podemos tener la tentación de realizar el cambio:

    1- for i = 0 to N-1
    2- 		co(i = 0 to N-1)
    3- 			c[i,j] = 0
    4- 			for k = 0 to N-1
    5- 				c(i,j) = c(i,j) + A[i,k] * B[k,j]
       		oc

que, aunque sea correcto, es innecesariamente inefeciente, dado que calcula N veces el valor de cada entrada. Es necesario un cambio en el orden de los bucles, de forma que el resultado sea:

    1- co(i = 0 to N-1)
    2- 		for i = 0 to N-1

##### Concurrencia por entradas
Estudiamos ahora una solución que permite la creación de $NxN$ procesos. Una solución de la forma

    1- co(i = 0 to N-1)
    2- 		co(i = 0 to N-1)
    3- 			c[i,j] = 0
    4- 			for k = 0 to N-1
    5- 				c(i,j) = c(i,j) + A[i,k] * B[k,j]
       		oc
       oc

tiene el problema de implicar la creación de procesos *lanzadera*, que tienen la única función de crear otros N procesos. Por esta raón, resulta conveniente introducir otra notación para `co` con dos argumentos

    1- co(i = 0 to N-1, i = 0 to N-1)
    2- 		c[i,j] = 0
    3- 		for k = 0 to N-1
    4- 			c(i,j) = c(i,j) + A[i,k] * B[k,j]
       oc

que permita la ejecución de todos los procesos en paralelo. No es correcto reemplazar el `for` de la línea 3 por un `co`, dado que los procesos así creados sí estarían en una situación de interdependencia y se producirán condiciones de carrera.

#### Concurrencia por bloques
Vamos a suponer que $N$ es múltiplo de 3, a fin de que todos los procesos trabajen el mismo tiempo. Queremos hacer una división de los cálculos como sigue:
	
					P1
					P1
					P1
	----------
					P2
					P2
					P2
	----------
					P3
					P3
					P3
					
Veamos como hacerlo utilizando la instrucción `process`:

	process MultP(w = i = 00 to P-1)
		for i = 0 to N/P-1
	 		for j = 0 to N-1
	 			i* = w * N/P + i
				c[i*,j] = 0
				for k = 0 to N-1
					c(i*,j) = c(i*,j) + A[i*,k] * B[k,j]
					
para utilizar la instrucción `co` podemos cambiar la primera línea por `co(i = 0 to P-1)` y añadir un punto de sincronización `oc` al final.

En ocasiones, existen problemas cuya solución iterativa no es afín a la concurrencia, por haber interdependencia entre las distintas iteraciones. Una posible solución es pasar a una solución recursiva y tratar de encontrar llamadas recursivas independientes. Veamos un ejemplo de este tipo.

### Ejemplo: cálculo de una integral entre dos puntos
Tenemos una función $f(x)$ definida en un intervalo $[a,b]$. Queremos calcular el área por debajo de la curva $f(x)$ en dicho intervalo. Para aproximar dicho valor, dividimos el intervalo en $N$ subintervalos y calculamos el área del trapezoide delimitado en cada subintervalo.

Solución iterativa del problema:
	
	área = 0;
	izq = f(a);
	ancho = (b-a) / N
	for x = A + ancho to B by ancho
		fder = f(x)
		área = área + (fder + fizq) * ancho / 2
		fizq = fder

El uso de la variable `área` como acumulador impide introducir concurrencia de forma directa. Presentamos aquí una solución recursiva (distinta):
	
	quad (left, right, lrarea)
		mid = (left + right) / 2
		fmid = f(mid)
		larea = (fleft + fmid) * (mid - left) / 2
		rarea = (fmid + fright) * (right - mid) / 2
		if (abs((larea + rarea) - lrarea) > EPSILON)
			larea = quad(left, mid, larea)
			rarea = quad(mid, right, rarea)
		return larea + rarea
		
Una solución concurrente (atención al uso del operador `||`):

	quad (left, right, lrarea)
		mid = (left + right) / 2
		fmid = f(mid)
		larea = (fleft + fmid) * (mid - left) / 2
		rarea = (fmid + fright) * (right - mid) / 2
		if (abs((larea + rarea) - lrarea) > EPSILON)
			co {quad(left, mid, larea) || 
				quad(mid, right, rarea)}
			oc
		return larea + rarea

# Conceptos básicos
Estudiaremos ahora los temas principales de la programación concurrente. A diferencia del mero paralelismo, los programas ejecutados concurrentemente dependen de componentens compartidos como variables compartidas o canales de comunicación.

La presencia de estos elementos exige introducir *sincronización* de alguna manera entre los procesos.
Por ahora, veamos varias maneras de introducir concurrencia en algunos problemas representativos.

### Ejemplo: Programación distribuida
Supongamos que queremos calcular el producto de dos matrices  $A, B$ mediante un programa que involucra a un coordinador y varios trabajadores. Los trabajadores reciben la fila i-ésima de la matriz $A$ y la matriz $B$ del coordinador y devuelven la fila i-esima del producto $AB$ al coordinador. El programa ejecutado por cada nodo es similar al siguiente:

	process Coord
		A[N, N] = iniA()
		B[N, N] = iniB()
		C[N, N]
		for i = 0 to N-1
			send(fila i de A) to worker[i]
			send B to writer[i]
		for i = 0 to N-1
			receive(fila i de C) from worker[i]
			
	process Worker[i = 0 to N-1]
		A[N]
		B[N, N]
		C[N]
		receive A y B de Coord
		for j = 0 to N-1
			c[j] = 0
			for k = 0 to N-1
				c[j] = c[j] + A[k] * B[k,j]
		send C to Coord
		
Estudiemos ahora una solución sin coordinador. Asumimos que cada trabajador ha inicializado ya A y B con su fila y su columna respectivamente. Supondremos que cada trabajador dispone únicamente de una copia de la fila i-ésima de A y de la columna i-ésima de B, con lo que debe comunicarse con los demás para obtener su columna de B y poder calcular toda la fila.

	process Worker[i = 0 to N-1]
		A[N]			// inicializado con fila i
		B[N]			// inicializado con columna i 
		C[N]
		
		anterior = i-1 mod N
		siguiente = i+1 mod N

		for j = 0 to N-1
			if (j != 0) 
				receive B from worker[anterior]
				
			j* = (i - j) mod N
			for k = 0 to N-1
				c[j*] += A[k] * B[k]
				
			if (j != N-1)
				send B to worker[siguiente]
		print C 
	

### Ejemplo: encontrar patrones en un archivo
Deseamos encontrar una cadena determinada en un archivo de texto. Una solución lineal puede programarse de la siguiente forma:
	
	string line;
	read a line of input from stdin into line;
	while (!EOF) {
		look for pattern in line;
		if (pattern is in line)
			write line;
		read next line of input;
	}
	
Otra solución puede pasar por utilizar dos procesos disjuntos:

	string line1, line2;
	read a line of input from stdin intro line1;
	while (!EOF) {
		co look for pattern in line1;
			if (pattern is in line 1)
				write line1;
		// read next line of input into list2;
		oc;
	}

Mientras que una solución que utiliza sincronización entre hilos podría ser:

	string buffer;
	bool done = false;
	
	co
		string line1;
		while (true) {
			wait for buffer to be full or done to be true;
			if (done) break;
			line1 = buffer;
			signal that buffer is empty;
			look for pattern in line1;
			if (pattern is in ine 1)
				write line1;
		}
		
		string line2;
		while (true) {
			read next line of input into line2;
			if (EOF) { done = true; break; }
			wait for buffer to be empty;
			buffer = line2;
			signal that buffer is full;
		}
	oc
	
### Ejemplo: cálculo del máximo

Supongamos que queremos paralelizar la búsqueda del máximo en un vector. Claramente la opción:
	
	int max = 0;
	co (i = 0 to N-1 )
		if (a[i] > max)
			max = a[i]
	oc

es incorrecta, mientras que la opción:

	int max = 0;
	co (i = 0 to N-1 )
		<if (a[i] > max)
			max = a[i]>
	oc

no gana nada en términos de eficiencia, pues los hilos ejecutan sus instrucciones secuenciamente. Una opción es utilizar *double check* y escribir:

	int max = 0;
	co (i = 0 to N-1 )
		if (a[i] > max)
			<if (a[i] > max)
				max = a[i]>
	oc
	
con lo que pueden conseguirse ganancias de eficiencia eliminando la posibilidad de que haya condiciones de carrera.

### Ejemplo: condiciones de carrera
Consideremos un programa del tipo:

	int y = 0, z = 0;
	co x = y + z, y = 1, z = 2 oc;
	
¿Qué posibles valores pueden tener las variables al término de la ejecución?

* `x = 0, y = 1, z = 2`
* `x = 1, y = 1, z = 2`
* `x = 2, y = 1, z = 2`
* `x = 3, y = 1, z = 2`

## Atomicidad
El caso anterior nos lleva a interesarnos por la propiedad *at-most-once* y el concepto de unicidad. Introducimos primero el concepto de *referencia crítica* como aquella referencia que es modificada por otro proceso.

Decimos que una expresión `x = e` cumple la propiedad *at-most-once* si:

* `e` contiene como máximo una referencia crítica y `x` no es leída por otros procesos.
* `e` no contiene ninguna referencia crítica (y, posiblemente, es leída por otros procesos)

Si una expresión `x = e` cumpe la propiedad *at-most-once* entonces la ejecución del programa no cambia si la asignación se hace de manera atómica (es decir, la ejecución de `x = e` es equivalente a la de `<x = e>`.

Si una instrucción no cumple la propiedad de *at-most-once* es necesario ejecutarla de manera atómica.

### Ejemplo:
Consideremos los programas:

	int x = 0, y = 0;
	co x = y + 1, y = x + 1 oc;
	
y

	int x = 0, y = 0;
	co <x = y + 1>, <y = x + 1> oc;

Dado que las asignaciones no cumplen la propiedad *at-most-once* ambos programas **no** son equivalentes. En concreto, el primer programa, a diferencia del segundo, puede acabar en el estado  `x = 1, y = 1`.

## Instrucción *await*
La expresión *await* espera a la terminación de otro proceso. Utilizando la expresión *await* y `<>` podemos implementar diversas estrategias:

* Espera activa: `<await(B);>`
* Exclusión mutua y sincronización: `<await(B) S;>`
* Exclusión mutua: `<S;>`

Si B cumpe la propiedad *at-most-once* entonces las instrucciones `<await (B);>` y `while (not B);` son **equivalentes**.

### Ejemplo: productor/consumidor utiizando *await*
Planteamos una solución al problema del productor consumidor utilizando la instrucción *await*:

	int buf;
	int p = 0, c = 0;
	process Productor {
		int a[N];
		while (p < N) {
			<await(p == c)>;
			buf = a[p];
			p += 1;

		}
	}
	
	process Consumidor {
		int b[N];
		while (c < N) {
			<await(p == c)>;
			b[c] = buf;
			c += 1;
		}
	}
	
## Lógica de programación
La lógica de programación es un sistema lógico formal para establecer y demostrar propiedades de los programas.

La fórmula básica es $ \{ P \} S \{ Q \} $, que denota que si la ejecución empienza en un estado que satisface P, la ejecución de S termina en un estado que satisface Q. Las reglas básicas de la lógica de programación son

![image info](fig/reglas.png)

Las reglas para `await` y `co` nos permiten introducir estas instrucciones cuando sea necesario:

![image info](fig/reglas2.png)

## Propiedades de seguridad
Los programas concurrentes deben tener fundamentalmente dos propiedades:

* Viveza: todos los procesos tienen oportunidad de ejecutarse. La *política de planificación* tendrá que determinar qué proceso ejecutar cuando haya varios posibles. Hay varios grados de jusiticia:
	* Incondicional: toda acción atómica es eventualmente ejecutada (round-robin o scheduling paralelo)
	* Débil: es incondicionalmente justa y además cualquier acción atómica condicional `<await(B) S;>` se ejecuta en algún momento, siempre que B se vuelva cierta y permanezca así hasta que se compruebe.
	* Fuerte: es incondicionalmente justa y cualquier acción atómica condicional `<await(B) S;>` se ejecuta en algún momento, siempre que B se vuelva cierta un número infinito de veces
	
Algunos comentarios:

* Alternar acciones entre dos procesos es fuertemente justa. También es extremadamente ineficiente.
* La mayoría de técnicas de utilidad (*round robin*, *time slicing*, ejecución en paralelo) no son fuertemente justas.

## Buenas propiedades
Presentamos aquí, como complemento a lo anterior, varias propiedades que deben tener los programas concurrentes, entre paréntesis si se trata de una propiedad de seguridad o de viveza:

* Corrección parcial (seguridad): si el programa terminal, el resultado es correcto.
* Exclusión mutua (seguridad): varios procesos no ejecutan secciones críticas a la vez.
* Terminación (viveza): el programa termina, todas las trazas son finitas.
* En algún momento se entra en la sección crítica (viveza).

La propiedad por antonomasia es la corrección total, que es la suma de corrección parcial y terminación.

# Tema 2
## Cerrojos y barreras
Pasamos a estudiar el problema de la sección crítica: $n$ procesos ejecutan un programa de la forma:

	process CS(i = 1 to n)
	while (true) {
		entry protocolo;
		SC;
		exit protocolo;
		no SC;
	}
	
Desde un punto de vista puramente conceptual, es suficiente con sustituir `SC` por `< SC >` para resolver el problema. Por supuesto, esto no hace más que retrasar el problema de cómo implementar en la práctica el operador `<>`
	
Nuestro objetivo, por tanto, es diseñar protocolos `entry` y `exit` que garanticen:

* Seguridad:
	* Exclusión mutua
	* Ausencia de bloqueo e interbloqueo
	* Ausencia de retraso innecesario: ningún proceso esperará para entrar en la SC si no hay procesos ya ejecutándose.
* Entrada eventual

siendo la primera la propiedad fundamental.

Presentamos una posible solución mediante una variable $lock = in_1 \vee in_2 \vee \dots \vee in_n$.

	--- (*) ---
	bool lock = false;
	process CS1
	while (true) {
		< await (!lock) lock = true; >
		SC;
		lock = false;
		no SC;
	}

### Implementación de await: técnicas
#### Test-and-set
La mayoría de procesadores disponen de instrucciones especiales que les permiten realizar una comprobación como la que aquí se realiza, a fin de implementar la anterior instrucción `< await >`. Una de estas instrucciones es `Test-and-Set` (TS) que tiene el siguiente aspecto:

	bool TS(bool lock) { <
		bool initial = lock;
		lock = true;
		return initial; 
	> }

El programa (*) con `<>` implementado mediante `TS`:

	bool lock = false;
	process CS1
	while (true) {
		while (TS(lock)) skip;
		SC;
		lock = false;
		no SC;
	}

tiene las propiedades buscadas salvo posiblemente la entrada eventual, que depende de la justicia del algoritmo de planificación. Si el algoritmo de planificación es débilmente justo (lo más común) un proceso podría permanecer perpetuamente esperando en el protocolo de entrada mientras haya otros procesos entrando en la sección crítica. 

Un importante problema es que los procesos deben continuamente comprobar el lock, ocupando ciclos de procesador y creando problemas de *competencia de memoria* al necesitar accederse a la copia de la variable en memoria (pues las copias en cada caché son continuamente invalidadas al escribirse en la variable `lock`).

#### Test-and-test-and-set
Veamos una interpretación distinta de (*):

	bool lock = false;
	process CS1
	while (true) {
		while (lock) skip;			// damos vueltas mientras lock este oon
		while (TS(lock)) {			// intentamos tomar lock
			while (lock) skip;		// si fallamos, seguimos dando vueltas
		}
		SC;
		lock = false;
		no SC;
	}

En este caso, solo se intenta tomar `lock` cuando existe la posibilidad de tomarla. En los dos bucles adicionales, `lock` solo es leída con lo que su valor puede leerse directamente de la caché local, sin necesidad de competir por la memoria.

#### Implementación de `await`
Como ya adelantábamos, estas técnicas puede utilizarse para implementar `<S;>` como `CSenter; S; CSexit` y para implementar `<await (B) CS; >` como:

	CSentry;
	while (!B) {
		CSexit;
		Delay;			// es preferible una esperar para evitar competir por memoria
		CSentry
	}
	CS;
	CSexit;
	
### Soluciones justas
#### Algoritmo tie-breaker
Las anteriores soluciones solo pueden asegurar la entrada eventual en el caso de que el planificador sea fuertemente justo. En general, no obstante, la mayoría de algoritmos de planifcación de utilidad práctica son solo débilmente justos.

Presentamos ahora varias soluciones que solo necesitan que el planificador sea débilmente justo.

En el algoritmo tie-breaker (creado para gestionar el acceso de dos procesos a la sección crítica) se mantiene una variable adicional que mantiene constancia de que proceso fue el último en acceder a la sección crítica. De esta forma, si dos procesos intentan entrar a la vez en la sección crítica se dará preferencia al que no haya entrado en último lugar.

	bool in1 = false, in2 = false;
	int last = 1;
	
	process CS1 {
		while (true) {
			in1 = true; last = 1;
			while (!in2 or last == 2) skip;
			critical section;
			in1 = false;
			noncritical section;
		}	
	}
	
	process CS2 {
		while (true) {
			in2 = true; last = 2;
			while (!in1 or last == 1) skip;
			critical section;
			in2 = false;
			noncritical section;
		}	
	}

Existen versiones de este algoritmo para $n$ procesos, pero son significativamente más complicadas.

#### Algoritmo del ticket
El algoritmo del ticket es mucho más fácil de generalizar al caso de $n$ procesos. La idea es similar a la de los mercados, en los que uno saca un número cuando llega y los clientes van siendo atendidos en función del número que tengan.

	int number = 1, next = 1, turn = [0, ..., 0]
	
	process CS(i = 1 to n) {
		while (true) {
			< turn[i] = number; number = number + 1; >
			< await (turn[i] == next) >
			critical section;
			< next = next + 1; >
			noncritical section;
		}
	}

Para poder implementar la instrucción `< turn[i] = number; number = number + 1; >` es deseable que la máquina cuente con una instrucción *Fetch-and-Add* que devuelve el antiguo valor de la variable e incrementa o decrementa su valor en una única acción atómica. Dicha instrucción se leería en ese caso como `FA(number, 1)`.

Si no tuviéramos disponible una instrucción de este estilo, podríamos implementarla mediante una instrucción del tipo *Test-and-Set* como `CSenter; turn[i] = number; number = number+1; CSexit`. En este caso, el algoritmo ya no es justo, dado que nada asegura que un proceso no vaya a esperar indefinidamente para entrar en la sección crítica.

#### Algoritmo del panadero
Se trata de un algoritmo similar al algoritmo del ticket que no requiere ninguna instrucción especial para ser justo, aunque a cambio de un significativo aumento de complejidad. 

En este algoritmo los clientes que entran en la tienda toman un número mayor que cualquiera del que tienen los clientes que ya se encuentran allí. Como en el caso del algoritmo del ticket, el siguiente en ser atendido será el cliente con el menor número pero en este caso los clientes comprararán sus números entre ellos, en lugar de con un contador central	
	
	int turn[1:n] = [0, ..., 0]
	
	process CS(i = 1 to n) {
		while (true) {
			< turn[i] = max(turn[1:n]) + 1; >
			for (j = 1 to n such that j != i])
				< await (turn[j] == 0 or turn[i] < turn[j]); >
			critical section;
			turn[i] = 0;
			noncritical section;
		}
	}

Para ver cómo podríamos implementar dicho algoritmo, pensemos en el caso de dos variables. Tenemos dos procesos con un procedimiento de entrada:

	-- P1 --
	turn1 = turn2 + 1;
	while (turn2 != 0 and turn1 > turn2) skip;

	-- P2 --
	turn2 = turn1 + 1;
	while (turn1 != 0 and turn2 > turn1) skip;
	
con el problema evidente de que ninguna de estas instrucciones se ejecutan de manera atómica, con lo que ambos procesos podrían poner su variable `turn` a 1 y entrar en la sección crítica al mismo tiempo.
	
Para evitar condiciones de carrera, podemos modificar el código para que tenga el aspecto:

	-- P1 --
	turn1 = 1; turn1 = turn2 + 1;
	while (turn2 != 0 and turn1 > turn2) skip;
	
	-- P2 --
	turn2 = 1; turn2 = turn1 + 1;
	while (turn1 != 0 and turn2 >= turn1) skip;
	
Ningún proceso puede ahora escapar del bucle while mientras el otro esté alterando su valor de `turn`, con lo que se previenen condiciones de carrera. En el caso de que ambos procesos intenten acceder a la sección crítica se da preferencia al proceso 1.

Para hacer el proceso simétrico podemos utilizar el orden lexicográfico, lo que nos permite generalizarlo para un número de procesos arbitrario y reescribir el algoritmo como:

	int turn[1:n] = [0, ..., 0]
	
	process CS(i = 1 to n) {
		while (true) {
			turn[i] = max(turn[1:n]) + 1;
			for (j = 1 to n such that j != i])
				while (turn[j] != 0 and (turn[i],i) > (turn[j],j)) 
					skip;
			critical section;
			turn[i] = 0;
			noncritical section;
		}
	}
	
### Barreras
Es muy común que un problema se resuelve realizando iteraciones que computan soluciones progresivamente más precisas. En cada iteración, el problema original se parte en subproblemas que pueden ser resueltos independientes para luego combinar dichas soluciones.

Una característica fundamental de la mayoría de soluciones paralelas iterativas es que cada iteración depende del resultado de la iteración anterior.

Una posible implementación puede ser:

	while (true) {
		co [i = 1 to n]
			code to implement task i;
		oc
	}
	
pero tiene el problema de que en cada iteración se crean $n$ procesos nuevos, lo que puede tener un coste muy alto. Es mucho más eficiente crear únicamente $n$ procesos y que se sincronicen al final de cada iteración.

	process Worker[i = 1 to n] {
		while (true) {
			code to implement task i;
		wait for all n tasks to complete;
		}
	}
	
Este modelo general de sincronización recibe el nombre de sincronización de barrera, porque los procesos no comienzan una nueva iteración hasta que todos los demás han concluido la suya.

Pasamos a estudiar distintas implementaciones de sincronización de barrera.

#### Contador compartido
La forma más sencilla de intentar implementar una barrera es como sigue:

	int count = 0;
	
	process Worker[i = 1 to n] {
		while (true) {
			code to implement task i;
			< count = count + 1; >
			< await (count == n); >
		}
	}

donde la barrera puede realizarse como:

	FA(count, 1);
	while (count != n) skip;

El problema de esta solución es que la variable `count` debe resetearse entre iteraciones y dicho reset debe hacerse antes de que ningún proceso realice una nueva ejecución.

En principio, esto puede solucionarse utilizando dos contadores, uno ascedente y otro descendente, que fueran alternando roles. Sin embargo, hay más inconvenientes: al actualizar continuamente el contador, las cachés deben invalidarse y la necesidad de acceder al valor de `count` en memoria lleva a eventos de competencia entre procesos.

#### Trabajadores y coordinadores
Para evitar la competencia por la memoria, podemos utilizar un array `arrive[1:n]` en el que cada proceso indique si ha terminado su ejecución. Tendríamos de esta forma la equivalencia 

	count == arrive[1] + ... + arrive[n]

Para evitar que cada proceso tenga que consultar continuamente el array `count` y volver a sumar los elementos, podemos introducir un proceso adicional `Coordinator` y un array adicional `continue[1:n]`. El coordinador se encarga de indicar a los `Worker`que pueden continuar.

En general, a la hora de trabajar con banderas como `arrive` y `continue` conviene recordar dos útiles principios:

1. El proceso que espera a que la bandera se active es el responsable de desactivarla.
2. Una bandera no debe activarse hasta estar seguros de que se ha desactivado previamente.

La solución, aplicando estos dos principios, tiene el aspecto:

	int arrive[1:n] = [0, ..., 0]
	int continue[1:n] = [0, ..., 0]
	
	process Worker[i = 1 to n] {
		while (true) {
			code to implement task i;
			arrive[i] = 1;
			< await (continue[i] == 1); >
			continue[i] = 0;
		}
	}
	
	process Coordinator {
		while (true) {
			for [i = 1 to n] {
				< await (arrive[i] == 1); >
				arrive[i] = 0;
			}
			for [i = 1 to n] continue[i] = 1;
		}
	}

Hay dos problemas en esta solución. El primero es que es necesario un proceso adicional coordinador. Lo ideal sería que este proceso se ejecutara en su propio procesador, a fin de que la espera activa de los trabajadores no interrumpiera su actividad, pero eso implica que se desaproveche un procesador que podría usarse para realizar trabajo.

El segundo problema es que el tiempo de ejecución del proceso coordinador depende proporcionalemtne del número de trabajadores. Aunque lo más probable es que todos los trabajadores acaben más o menos al mismo tiempo (pues ejecutan el mismo código), el coordinador va ciclando de bandera en bandera, esperando a que se activen.

La solución pasa por hacer que cada trabajador se comporte a la vez como un coordinador, disponiéndoles en una estructura arborescente.

![](fig/tree.png) 

#### Barreras en árbol
En el árbol, cada proceso combina los resultados de sus hijos y se los pasa a la raíz. Esta disposición es mucho más eficiente para un número grande de procesos, al ser la altura del árbol proporcional a $\log_2(n)$.

##### Hoja L
	
	arrive[L] = 1;
	< await (continue[i] == 1) >;
	continue[L] = 0;
	
##### Interior I
	
	< await (arrive[left(I)] == 1); >
	arrive[left(I)] = 0;
	< await (arrive[right(I)]) == 1); >
	arrive[right(I)] = 0
	arrive(I) = 1;
	< await (continue[I] == 1); >
	continue[I] = 0;

##### Raiz R

	< await (arrive[left(R)]) == 1); >
	arrive[left(R)] = 0;
	< await (arrive[right(R)] == 1); >
	arrive[right(R)] = 0;
	continue[left(R)] = 1;
	continue[right(R)] = 1;
	
#### Barreras simétricas
Las barreras simétricas surjen al intentar encontrar soluciones en las que todos los procesos se comporten de forma simétrica.
Supongamos que tenemos dos procesos con identificador $i$, $j$.

Una solución directa podría tener el aspecto:

	-- Proceso i
	1. arrive[i] = 1;
	2. < await (arrive[j] == 1); >
	3. arrive[j] = 0;

	-- Proceso j
	4. arrive[j] = 1;
	5. < await (arrive[i] == 1); >
	6. arrive[i] = 0;

pero se trata de una solución **incorrecta**, pues el orden de ejecución 1, 4, 2, 5, 3, 1, 6  hará que $j$ crea que $i$ ha hecho una única iteración, cuando en realidad ha ejecutado dos iteraciones.

Para ello, podemos añadir una instrucción extra:

	-- Proceso i
	< await (arrive[i] == 0); >
	arrive[i] = 1;
	< await (arrive[j] == 1); >
	arrive[j] = 0;
	
	-- Proceso j
	< await (arrive[j] == 0); >
	


##### La barrera mariposa
Parte de que tenemos $2^n$ procesos (rellenando con procesos vacíos si fuera necesario).

Vamos a emparejar los procesos de dos en dos

	