package com.company;


import java.util.ArrayList;
import java.util.List;

public class LockRompeEmpate {
    public  int M;
    public volatile List<Integer> in, last;
    public volatile int n = 0;
    public int T = 50;

    public LockRompeEmpate() {
        this(100);
    }

    public LockRompeEmpate(int M) {
        this.M = M;
        this.in = new ArrayList<>(2*M);
        this.last = new ArrayList<>(2 * M);
        for (int i = 0; i < 2*M; ++i) {
            this.in.add(-1);
            this.last.add(-1);
        }
    }

    public void increase ( int i){
        for (int k = 0; k < T; ++k) {
            takeLock(i);
            n++;
            releaseLock(i);
        }
    }

    public void decrease ( int i){
        for (int k = 0; k < T; ++k) {
            takeLock(i);
            n--;
            releaseLock(i);
        }
    }

    public void takeLock ( int i){
        for (int j = 0; j < 2 * M; ++j) {
            last.set(j, i);
            in.set(i, j);
            for (int k = 0; k < 2 * M; ++k)
                if (i != k)
                    while (in.get(k) >= in.get(i)
                            && last.get(j) == i) ;

        }
    }

    public void releaseLock ( int i){
        in.set(i, -1);
    }


    public static void main (String[] args) throws InterruptedException {
        int M = 2;
        LockRompeEmpate alg = new LockRompeEmpate(M);
        List<Thread> incrementadores = new ArrayList<>();
        List<Thread> decrementadores = new ArrayList<>();
        Thread newThread;
        for (int i = 0; i < 2*M; ++i) {
            int finalI = i;
            if (i % 2 == 0) {
                newThread = new Thread(() -> {alg.increase(finalI);});
                newThread.start();
                incrementadores.add(newThread);
            }
            else {
                newThread = new Thread(() -> {alg.decrease(finalI);});
                newThread.start();
                decrementadores.add(newThread);

            }
        }

        for (int i = 0; i < M; ++i) {
            incrementadores.get(i).join();
            decrementadores.get(i).join();
        }

        System.out.println("Value of n: " + alg.n);
    }
}
