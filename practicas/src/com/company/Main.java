package com.company;


import java.util.ArrayList;
import java.util.List;

public class Main {
    public static final int M = 10000;
    public static volatile int n = 0;
    public static boolean in1 = false,  in2 = false;
    public static volatile int last = 1;

    public static void increase() {
        for (int i = 0; i < M; ++i) {
            last = 1;
            in1 = true;
            while (in2 && last == 1) ;
            n = n + 1;
            in1 = false;
        }
    }

    public static void decrease() {
        for (int i = 0; i < M; ++i) {
            last = 2;
            in2 = true;
            while (in1 && last == 2) ;
            n = n-1;
            in2 = false;
        }
    }


    public static void main(String[] args) throws InterruptedException {
        Thread t1, t2;

        t1 = new Thread(() -> {increase();});
        t2 = new Thread(() -> {decrease();});

        t1.start();
        t2.start();

        t1.join();
        t2.join();
        System.out.println("Value of n: " + n);
    }
}
