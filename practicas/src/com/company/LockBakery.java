package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class LockBakery {

    public int M;
    public volatile ArrayList<Integer> turn;
    public volatile int n = 0;
    public volatile int T = 500;

    public LockBakery() {
        this(100);
    }

    public LockBakery(int M) {
        this.M = M;
        this.turn = new ArrayList<>(2*M);
        for (int i = 0; i < 2*M; ++i) {
            this.turn.add(-1);
        }
    }

    public void increase ( int i){
        for (int j = 0; j < T; ++j) {
            takeLock(i);
            n = n + 1;
            releaseLock(i);
        }
    }

    public void decrease ( int i){
        for (int j = 0; j < T; ++j) {
            takeLock(i);
            n = n - 1;
            releaseLock(i);
        }
    }

    public void takeLock ( int i){
        turn.set(i,1);
        turn.set(i, Collections.max(turn)+1);
        for(int j=0; j<2*M; j++){
            while(turn.get(j) != -1 && (turn.get(i)>turn.get(j) || (turn.get(i)==turn.get(j) && i>j)));
        }
    }

    public void releaseLock ( int i){
        turn.set(i                                        ,        -1);
    }

    public static void main (String[] args) throws InterruptedException {
        int M = 10;
        LockBakery alg = new LockBakery(M);
        List<Thread> incrementadores = new ArrayList<>();
        List<Thread> decrementadores = new ArrayList<>();
        Thread newThread;
        for (int i = 0; i < 2*M; ++i) {
            int finalI = i;
            if (i % 2 == 0 ) {
                newThread = new Thread(() -> {alg.increase(finalI);});
                newThread.start();
                incrementadores.add(newThread);
            }
            else {
                newThread = new Thread(() -> {alg.decrease(finalI);});
                newThread.start();
                decrementadores.add(newThread);

            }
        }

        for (int i = 0; i < M; ++i) {
            incrementadores.get(i).join();
            decrementadores.get(i).join();
        }

        System.out.println("Value of n: " + alg.n);
    }

}
